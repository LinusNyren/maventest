import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LogicTest {
	
	public int division(int a, int b) {
		return a/b;
	}
	public int addition(int a, int b) {
		return a+b;
	}
	public int subtraction( int a, int b) {
		return a-b;
	}
	@Test
	public void divisionTest() {
		assertEquals(5, division(10, 2));
	}
	@Test
	public void additionTest() {
		assertEquals(12, addition(10, 2));
	}
	@Test
	public void subtractionTest() {
		assertEquals(8, subtraction(10, 2));
	}
}

